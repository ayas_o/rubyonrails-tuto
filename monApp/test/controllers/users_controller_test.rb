require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:michael)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

"""
  test 'should create user' do
    assert_difference('User.count') do
      post :create, user: { email: @user.email, name: @user.name}
    end

    assert_redirected_to user_path(assigns(:user))
  end
"""

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end
"""
  test 'should get edit' do
    get :edit, id: @user
    assert_response :success
  end

  test 'should update user' do
    patch :update, id: @user, user: { email: @user.email, name: @user.name }
    assert_redirected_to user_path(assigns(:user))
  end
"""
end
