require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.new(name: "user2", email: "use2r@mail.fr",
                      password: "foobar", password_confirmation: "foobar")
  end

  test 'Should be valid' do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name="     "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email="   "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a"*51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a"*250+"@example.fr"
    assert_not @user.valid?
  end
"""
  test 'email validation should accept valid adresses' do
    adresses = %w[user@example.fr AUB.dc@mail.com A_US-er@example.org alice+bob@baz.cn]
    adresses.each do |valid_adress|
        @user.email=valid_adress
        assert @user.valid?, valid_adress.inspect should be valid
        # variable valid address #
    end
  end
"""
  test "email validation should reject invalid adresses" do
    adresses = %w[user@example,fr AUB.dc@@mail.com A_US-erexample.org alice@bob+bobbaz.cn]
    adresses.each do |valid_adress|
        @user.email=valid_adress
        assert_not @user.valid?, "#{valid_adress.inspect} should be invalid"
    end
  end

  test "email adresses should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test "paswword should be present" do
    @user.password=@user.password_confirmation="   "
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password=@user.password_confirmation= "a"*5
    assert_not @user.valid?
  end

end
