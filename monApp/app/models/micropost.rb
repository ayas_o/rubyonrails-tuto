class Micropost < ActiveRecord::Base
  validates :content, presence: true,  length: { maximum: 140}
  validates :user_id, presence: true
  validate :picture_size

  mount_uploader :picture, PictureUploader
  belongs_to :user

  default_scope -> { order(created_at: :desc) }

  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5MB")
    end
  end

end
