class AccountActivationMailer < ActionMailer::Base
  default from: "noreply@example.com"
  layout 'mailer'
end
